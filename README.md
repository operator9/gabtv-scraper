GabTV Statistics Scraper
========================

**UPDATE**

We're making the switch to a standalone client that can be found here:
https://codeberg.org/operator9/gabtv-scraper-client

This repository won't be updated with more data. You are free to run it as you wish however, and can
still use it to collect data from within the browser.

The new repository runs as a standalone client and doesn't depend on any browser. It will
automatically merge data and provide a single command to scrap, produce graphs and so forth

---

Scraper script running in the browser (via TamperMonkey or equivalent) to collect data from Gab TV
for statistical purposes.

The script is intended for researchers. It's only a transitional package until a standalone version
is developed.

This repo is not associated with Gab, nor is it part of any affiliation.

Features
--------

- Scrape GabTV site (no need to log in) for metadata of available videos in categories (currently 6
  months back from and including current month)
- Updates and merge view count using full or incremental sets (limited to above restriction)
- Various data analytics queries embedded in the SQLite database
- Produce PNG, JPEG, SVG or PDF reports from the analytics data
- Exchange formats between SQLite, CVS and JSON (on export, SQLite->CSV with tool)
- Search video URLs based on title and/or username

Instructions
------------

The script runs in the browser via extensions such as TamperMonkey. It's specifically developed for
TamperMonkey, it may or may not work with similar extensions.

It runs on Chromium based browsers (Chrome, Brave, Opera, Vivaldi etc.) and will likely require
slight modifications to run in Firefox.

- Install TamperMonkey if you haven't already. It's available from Chrome Store.
- Use the extension icon, go to Dashboard and create a new user script
- Remove any existing code and paste all the code from this repo (`/tamper-monkey/browser.js`) into
  the code pane.
- Hit CTRL + S to save. You are now ready to operate the script

Open console when on tv.gab.com and go to the categories tab. You should now see a menu in the
console with commands you can run.

**Alternative Installation**

If you're using TamperMonkey for the first time, simply import the file
`tamper-monkey/tampermonkey-settings.txt` via the extension's Dashboard > Utilities. This contains
all settings necessary to run the script.

*Important: do not run other/external user-scripts with these settings. The included settings are
specifically for running the GabTV scraper only.*

**Commands**

`run([limit])` will start the automated process of scraping each category page, collect stats until
all categories have been scanned. This is fully automated. Note that all previous data will be
cleared.

Once that is complete the data will be stored in `localStorage` (note: this will be moved to
IndexedDB in the future due to storage limits). You can collect and export this data as CSV, JSON or
SQLite database files.

The `limit` options allow you to produce a smaller dataset that can be accumulated with an existing
one. The limit is given as text: "3 hours ago" (hours: max 21), "a day ago", "2 days ago"
(days: max 25), "4 months ago" and so on. This does a *crude* abortion when a paginated page
contains a date older than this. This means there will be overlaps and even dates older than the
limit if a paginated page contains few videos spread over a longer period. *See utilities for
merging and accumulating these datasets into one.*

`abort()` is only useful when `run()` is in progress. This will abort the process and return to the
categories page. Collected data so far is kept.

`timer()` sets a timer for `run` to run at midnight (local time). To cancel a timer, run the command
again. The timer will only run once.

`exportSQL()` exports as SQLite database. Note, when this command runs it will load an external
scripts for the SQLite engine. The engine is loaded from a CDN, and you can download it directly
using the embedded links to verify its code.

`exportJSON()` exports as JSON file. The content is not formatted. If you want to read it on command
line or reformat it you can run (Linux):

    $ cat gabtv-stats.json | jq
    $ cat gabtv-stats.json | jq > gabtv-stats-formatted.json

`exportCSV()` exports as a column-separated file. Import into LibreOffice Calc or Microsoft Excel or
do whatever you want with the file.

NOTE:

The `exportJSON()` and `exportCSV()` commands are provided for convenience and should not be used
for daily scraping. Use `exportSQL()` (you can always add support for the other formats yourself).


Scraping
--------

To capture more accurate timestamps and view counts the script need to run daily, ideally at the
same time and as close to Gab's midnight. The process can be automated using solutions like
puppeteer, but this won't be covered here. You need to export data as SQLite for daily operation.

The script collects data points from each category page which currently includes:

- User ID (who owns the video)
- "date" - there is no real timestamp so for example "2 hours ago" is translated into an approximate
  timestamp.
- Number of views for that video
- Category
- Video ID (used to remove duplicates)
- View time in seconds
- URL-slug to video
- Last updated time

The files export with the following header or property names:

`category`, `dateISO`, `date`, `user`, `views`, `id`, `time`, `url` and `updated`.

In the JSON file the data points are found in the array:

    json.dataPoints[...];

Each of the data points is an object with property names as described above.

Not that Gab limits the available history of videos to six months. A consequence of this is that
when a new month starts you won't be able to update views for videos older than 6 months without
scraping each individual video. The `updated` field can be used to determined videos which falls
outside the 6 months window.

A full scraping will take roughly 30-35 minutes depending on various factors.

Save each scraped database per day and use the included merge tool `utils/mergedata.js` to compile
the different datasets and generate view deltas. The utility will take care of duplicate IDs and so
forth.


Modifying the Code
------------------

If you add new sql files to the `gabtv-data/sql` folder and want them to be inserted as views in the
exported SQLite file, run the script `utils/createviewsql.js` to produce a new single SQL statement
that you then insert into the `const viewsSQL = '...'` (use back-ticks for quotes) in the main
`tamper-monkey/browser.js` file.

Copy the final code over to TamperMonkey, save, and you should be ready to go.


Utilities
---------

The included utilities to merge/accumulate data for SQLite, JSON and CSV requires NodeJS to work.
Run 'npm i' is the project's root folder to install dependencies.

Run the util `node utils/<tool> --help` to get usage instructions.

To use the `utils/generategraphs.js` tool, see
the [requirements and prerequisites](https://github.com/Automattic/node-canvas/blob/master/Readme.md)
for your platform.

Use the `mergedata.js` to merge incremental or new data with an existing base database, and to
upgrade base to new table format if needed.


Searching for video URLs and usernames
--------------------------------------

The utility `utils/geturl.js` allows you to search video URLs using a keyword and/or a username.
Both the keyword and username can be partial, incl. video IDs. Multiple keywords are not supported
yet.

Example:

```bash
$ utils/geturl.js -d gabtv-data/gabtv-stats-base.sqlite lockdown  # results in:
Searching: "lockdown"
https://tv.gab.com/channel/throwthefirststone/view/london-anti-lockdown-protestors-huge-60cb10c4e5215219df4d913d
https://tv.gab.com/channel/realalexjones/view/lockdown-depression-causes-5-year-olds-60ca1e0e81cb818951b5d7b2
...
```

Searching for usernames using the `--list, -l` and `--username, -u` options:

```bash
$ utils/geturl.js -d gabtv-data/gabtv-stats-base.sqlite -u alex -l  # results in
Searching usernames containing "alex"
realalexjones
...
```

Combine username and keyword using the `--username, -u`:

```bash
$ utils/geturl.js -d gabtv-data/gabtv-stats-base.sqlite lockdown -u alex
Searching: "lockdown"
https://tv.gab.com/channel/realalexjones/view/lockdown-depression-causes-5-year-olds-60ca1e0e81cb818951b5d7b2
https://tv.gab.com/channel/realalexjones/view/alex-jones-warned-you-the-lockdowns-60286aad39ecb29588c0aeb0
...
```

How To Read And Query SQLite Databases
--------------------------------------

Install the free open source tool "DB Browser for SQLite". It's available for Windows, Mac and
Linux.

https://sqlitebrowser.org/

After installation, open the scraper database from it and run any SQL query you like in the
'Execute SQL' tab. The scraper database includes several predefined queries as views. The views can
be browsed in the 'Browse Data' tab. NOTE: These views may change over time.


TODO
----

- ~~Standalone version~~

License
-------

MIT (c) 2021 Operator9, swampcreature
