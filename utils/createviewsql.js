#!/usr/bin/env node
/*
 *  This script will create a single SQL query statement
 *  from the sql files in gabtv-data/sql. This is used
 *  only when new SQL files are added, and the result is
 *  inserted into the main code base.
 *
 */

'use strict';

const path = require('path');
const fs = require('fs');
const options = require('commander')
  .description('Simply run to produce a new SQL statement that you insert into the main browser.js file. See README file.')
  .parse(process.argv);

const sqlPath = path.join(__dirname, '..', 'gabtv-data', 'sql');

const sql = [];
try {
  fs.readdirSync(sqlPath).forEach(file => {
    const query = fs.readFileSync(path.join(sqlPath, file), 'UTF8')
      .replace(/\r/g, '')
      .split('\n')
      .map((line, i) => line.split('--')[ 0 ])
      .join(' ')
      .replace(/\s+/g, ' ');

    const name = file.substr(0, file.length - 4);
    const drop = `DROP VIEW IF EXISTS "${ name }";`;
    sql.push(drop);
    const view = `CREATE VIEW "${ name }" AS ${ query };`;
    sql.push(view);
  });
}
catch(err) {console.log(err);}

console.log(sql.join('\n').replace(/(;;|;\s;|\s;)/g, ';'));
