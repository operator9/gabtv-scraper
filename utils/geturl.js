#!/usr/bin/env node
/*
 *  Search in database and get URL to videos
 *
 *  (c) 2021 Operator9
 */

'use strict';

const options = require('commander')
  .usage('-d database.sqlite [-u username] searchtext')
  .option('-d, --database <pathtodb>', 'Path to scraped database.')
  .option('-u, --username <username>', 'Full or part of username. Limits search to this users')
  .option('-l, --list', 'List usernames instead of search, based on -u, --username')
  .option('--limit <rows>', 'Max number of result rows', 10)
  .parse(process.argv);

const args = options.opts();

if ( (!options.args.length && !args.list) || !args.database ) {
  return options.help();
}

let db;
try {
  db = require('better-sqlite3')(args.database, { readonly: true });
}
catch(err) {
  return console.log(`Could not open database: "${ args.database }": ${ err.message }`);
}

const user = args.username ? `%${ args.username }%` : '%';
const search = options.args[ 0 ] === '*' ? '%' : `%${ options.args[ 0 ] }%`;

if ( args.list ) {
  console.log(`\x1b[1;34mSearching usernames containing "\x1b[32m${ args.username }\x1b[34m":\x1b[m`);
  db.prepare('SELECT DISTINCT user FROM gabtv WHERE user LIKE ? LIMIT ?').all(user, args.limit)
    .forEach(row => {
      console.log(row.user);
    });

}
else {
  console.log(`\x1b[1;34mSearching: "\x1b[32m${ options.args[ 0 ] }\x1b[34m"\x1b[m`);
  db.prepare('SELECT user, url FROM gabtv WHERE user LIKE ? AND url LIKE ? LIMIT ?')
    .all(user, search, args.limit).forEach(row => {
    console.log(generateURL(row));
  });
}

function generateURL(row) {
  return `https://tv.gab.com/channel/${ row.user }/view/${ row.url }`;
}

db.close();