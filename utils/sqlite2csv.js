#!/usr/bin/env node
/*
 *  sqlite2csv
 *
 * Convert a scraper database to CSV file
 * (c) 2021 Operator9
*/

'use strict';

const options = require('commander')
  .usage('infile.sqlite outfile.csv')
  .parse(process.argv);

const args = options.args;

if ( args.length !== 2 || !args[ 1 ].toLowerCase().endsWith('.csv') ) {
  return options.help();
}

const inFile = args[ 0 ];
const outFile = args[ 1 ];
const fs = require('fs');
const csv = [ 'category;dateISO;date;user;views;id;time;url;updated' ];

let db;
try {
  db = require('better-sqlite3')(inFile, { readonly: true });
}
catch {
  return console.error(`Could not open SQLite database: "${ inFile }"`);
}

db.prepare('SELECT * FROM gabtv ORDER BY dateISO').all().forEach(row => {
  csv.push(`${ row.category };${ row.dateISO };${ row.date };${ row.user };${ row.views };${ row.id };${ row.time };${ row.url || '' };${ row.updated || '' }`);
});

db.close();

try {
  fs.writeFileSync(outFile, csv.join('\r\n'), 'UTF8');
}
catch(err) {
  return console.error(`Could not write to file: "${ outFile }": ${ err.message }`);
}

console.log(`Wrote ${ csv.length } lines to CSV file "${ outFile }"`);
