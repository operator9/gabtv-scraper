#!/usr/bin/env node
/*
 * Generate graphs from dataset 0.7.2-alpha
 *
 * Copyright (c) 2021 Operator9, swampcreature
 * License: AGPL
 */

'use strict';

const today = new Date().toISOString().substr(0, 10);
const yesterday = (new Date(Date.now() - 86400000)).toISOString().substr(0, 10);

const options = require('commander')
  .usage('gabtv.sqlite -f folder')
  .option('-f, --folder <path>', 'Folder to store generated PNG files.')
  .option('-t, --type <png|jpeg|pdf|svg>', 'Output PNG, JPEG, PDF or SVG format', 'png')
  .option('-m, --maxdate <isodate>', 'Render only up to and including this date ("today", yyyy-mm-dd)', yesterday)
  .option('-s, --smoothing <days>', 'Number of days for Weighted Moving Average (3-30)', '25')
  .option('--transparent', 'Use transparent background')
  .option('--single', 'Produce single page PDF file.')
  .option('--no-date', 'Don\'t use ISO date as prefix in filenames.')
  .parse(process.argv);

const args = options.opts();
if ( !args.folder || options.args.length !== 1 ) return options.help();

// translate max date
if ( args.maxdate.toLowerCase() === 'today' ) args.maxdate = today;
args.smoothing = Math.max(3, Math.min(30, args.smoothing | 0));

const path = require('path');
const fs = require('fs');
const folder = path.resolve(args.folder);

let db;
try {
  db = require('better-sqlite3')(options.args[ 0 ], { readonly: true });
}
catch {
  return console.error(`Could not open database "${ options.args[ 0 ] }"`);
}

const { createCanvas, loadImage } = require('canvas');
const fileType = [ 'pdf', 'svg', 'jpeg', 'jpg', 'png' ].includes(args.type.toLowerCase()) ? args.type.toLowerCase() : 'png';
const box = { x: 120, y: 90, w: 1720, h: 850 };
const views = [
  { name: 'Top Categories and Number of Videos', countCol: 'numOfVideos', labelCol: 'category', type: 'bar' },
  { name: 'Videos Posted per Month', countCol: 'numOfVideos', labelCol: 'month', type: 'bar', showMax: true },
  { name: 'View Deltas Per Day', title: 'View Deltas Per Day Last $COUNT days', countCol: 'Views', labelCol: 'Day', dayCol: 'Day', type: 'line', trend: true, wma: true },
  { name: 'Views Per Last 45 days', title: 'Number of Videos Last $COUNT days', countCol: 'Videos', labelCol: 'Day', dayCol: 'Day', reverse: true, type: 'line', trend: true, wma: true, append: ' - videos' },
  { name: 'Views Per Last 45 days', title: 'Number of Spread Views Last  $COUNT days', countCol: 'Views', labelCol: 'Day', dayCol: 'Day', reverse: true, type: 'line', trend: true, wma: true, append: ' - views' },
  { name: 'Views Per Last 45 days', title: 'Avg. Spread Views/Video Last $COUNT days', countCol: 'Avg Views/Video', labelCol: 'Day', dayCol: 'Day', reverse: true, type: 'line', trend: true, wma: true, append: ' - avg views pr. video' },
  { name: 'Views Per Last 90 days', title: 'Number of Videos Last $COUNT days', countCol: 'Videos', labelCol: 'Day', dayCol: 'Day', reverse: true, type: 'line', trend: true, wma: true, append: ' - videos' },
  { name: 'Views Per Last 90 days', title: 'Number of Spread Views Last  $COUNT days', countCol: 'Views', labelCol: 'Day', dayCol: 'Day', reverse: true, type: 'line', trend: true, wma: true, append: ' - views' },
  { name: 'Views Per Last 90 days', title: 'Avg. Spread Views/Video Last $COUNT days', countCol: 'Avg Views/Video', labelCol: 'Day', dayCol: 'Day', reverse: true, type: 'line', trend: true, wma: true, append: ' - avg views pr. video' },
  { name: 'Segmented Views Detailed Lower-End', countCol: 'count', labelCol: 'label', type: 'bar', transpose: true, showMax: true },
  { name: 'Misc stats', type: 'text', append: '', vertical: true }
];

let canvas;
let ctx;

/*--------------------------------------------------------------------------------------------------
 MAIN
--------------------------------------------------------------------------------------------------*/
go().then(db.close.bind(db)).catch(console.log);

async function go() {
  const img = await loadImage(getImage());

  views.forEach(view => {
    // recreate each time due to PDF/SVG not clearing context
    if ( !(args.single && fileType === 'pdf' && canvas) ) {
      canvas = createCanvas(1920, 1080, fileType === 'pdf' || fileType === 'svg' ? fileType : undefined);
      ctx = canvas.getContext('2d');
      ctx.antialias = 'subpixel';
    }

    if ( !args.transparent ) {
      ctx.fillStyle = '#fff';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    let min, max;
    let data = db.prepare(`SELECT * FROM "${ view.name }"`).all()
      .filter(row => view.dayCol ? row[ view.dayCol ].substr(0, 10) <= args.maxdate : true);

    if ( view.title ) view.title = view.title.replace(/\$COUNT/gi, data.length);  // note: mutates, but we run only once

    if ( view.type === 'text' ) {
      renderText(data, view);
    }
    else {
      if ( view.transpose ) {
        min = getMin(Math.min(...Object.values(data[ 0 ])));
        max = getMax(Math.max(...Object.values(data[ 0 ])));
        data = transpose(data);
      }
      else {
        // prepare doesn't support ? in selector, nor as table name
        min = getMin(db.prepare(`SELECT MIN("${ view.countCol }") min FROM "${ view.name }"`).get().min);
        max = getMax(db.prepare(`SELECT MAX("${ view.countCol }") max FROM "${ view.name }"`).get().max);
      }

      const step = getStep(min, max);

      if ( view.reverse ) data.reverse();

      const labels = data.map(row => row[ view.labelCol ]);

      render(min, max, step, labels, view.title || view.name, view.labelCol, view.countCol, data, view);
    }

    ctx.drawImage(img, 1830, 10, 70, 70);

    if ( args.single && fileType === 'pdf' ) {
      ctx.addPage();
    }
    else {
      saveFile(`${ view.name + (view.append || '') }.${ fileType }`);
    }
  });

  if ( args.single && fileType === 'pdf' ) {
    saveFile('GabTV-Reports.pdf');
  }
}

//--------------------------------------------------------------------------------------------------

function renderText(data, view) {

  addTimestamp(ctx);

  // title
  ctx.font = '24px sans-serif';
  ctx.fillStyle = '#000';
  ctx.textAlign = 'center';
  ctx.textBaseline = 'middle';
  ctx.fillText(view.title || view.name, canvas.width >> 1, 40);

  ctx.font = 'bold 22px sans-serif';

  const keys = Object.keys(data[ 0 ]);
  const values = Object.values(data[ 0 ]);

  let w1 = 0;
  let w2 = 0;
  keys.forEach((key, i) => {
    let w = ctx.measureText(key).width;
    if ( w > w1 ) w1 = w;
    w = ctx.measureText('' + values[ i ]).width;
    if ( w > w2 ) w2 = w;
  });

  // todo two columns support

  const height = 100;
  let y = box.y + 40;
  let x = view.vertical ? (1920 - (w1 + w2 + 80)) >> 1 : box.x;

  keys.forEach((key, i) => {
    const v = values[ i ].toLocaleString();

    if ( view.vertical ) {
      ctx.textAlign = 'right';
      ctx.fillStyle = '#05c';
      ctx.fillRect(x, y, w1 + 40, 32);
      ctx.fillStyle = '#fff';
      ctx.fillText(key, x + w1 + 20, y + 15);

      ctx.fillStyle = '#000';
      ctx.fillText(v, x + w1 + w2 + 80, y + 15);

      y += 40;
    }
    else {
      const w = Math.max(ctx.measureText(key).width, ctx.measureText(v).width) + 40;
      if ( x + w > box.x + box.w ) {
        x = box.x;
        y += height;
      }

      ctx.fillStyle = '#05c';
      ctx.fillRect(x, y, w, 32);
      ctx.fillStyle = '#fff';
      ctx.textAlign = 'center';
      ctx.fillText(key, x + w * .5, y + 15);

      ctx.fillStyle = '#000';
      ctx.textAlign = 'right';
      ctx.fillText(v, x + w - 10, y + 50);

      x += w + 20;
    }
  });

}

function render(minY, maxY, stepY, labels, title, xName, yName, data, view) {

  ctx.beginPath();

  addTimestamp(ctx);

  // title
  ctx.font = '24px sans-serif';
  ctx.fillStyle = '#000';
  ctx.textAlign = 'center';
  ctx.textBaseline = 'middle';
  ctx.fillText(title, 960, 40);

  // x-axis
  ctx.font = '20px sans-serif';
  ctx.fillText(xName, 960, 1040);

  // y-axis
  ctx.setTransform(1, 0, 0, 1, 20, 540);
  ctx.rotate(-Math.PI * 0.5);
  ctx.fillText(yName, 0, 0);

  ctx.textAlign = 'right';
  ctx.font = '12px sans-serif';
  ctx.fillStyle = '#000';

  // box offsets
  ctx.setTransform(1, 0, 0, 1, box.x + 0.5, box.y + 0.5);
  ctx.beginPath();
  ctx.lineWidth = 1;

  // draw vertical scale
  const divisor = maxY - minY === 0 ? 1 : maxY - minY;
  const scale = box.h / divisor;

  for(let y = minY; y < maxY; y += stepY) {
    drawLine(-10, _y(y), box.w, _y(y));
    ctx.fillText(y, -15, _y(y));
  }

  // draw horizontal scale
  const stepX = box.w / labels.length;
  for(let x = 0; x < box.w; x += stepX) {
    drawLine(x, _y(minY), x, _y(minY) + 10);
  }
  drawLine(box.w, _y(minY), box.w, _y(minY) + 10);

  ctx.strokeStyle = '#999';
  ctx.stroke();

  // labels
  let maxWidth = 0;
  let angle = 0;
  labels.forEach(label => {
    const w = ctx.measureText(label).width;
    if ( w > maxWidth ) maxWidth = w;
  });

  if ( maxWidth > stepX * 0.9 ) angle = -Math.PI * 0.2;

  const deltaX = view.type === 'bar' ? stepX * 0.5 : 0;

  // todo box intersection check for crowded label pools
  labels.forEach((label, i) => {
    ctx.setTransform(1, 0, 0, 1, box.x + 0.5 + i * stepX + deltaX, box.y + box.h + 15.5);
    ctx.rotate(angle);
    ctx.fillText(label, 0, 0);
  });

  ctx.setTransform(1, 0, 0, 1, box.x + 0.5, box.y + 0.5);

  const points = data.map((d, i) => {
    return {
      x: i * stepX,
      y: _y(d[ view.countCol ])
    };
  });

  // PLOT

  ctx.beginPath();

  if ( view.type === 'line' ) {

    if ( view.wma ) {
      ctx.fillStyle = '#aacd65';
      ctx.fillRect(-box.x + 30, -box.y + 30, 16, 16);
      ctx.fillStyle = ctx.strokeStyle = '#000';
      ctx.strokeRect(-box.x + 30, -box.y + 30, 16, 16);
      ctx.textAlign = 'left';
      ctx.fillText(`Weighted moving average (${ args.smoothing } days)`, -box.x + 56, -box.y + 38);
      drawPolygon(_toWMA(data), '#aacd65', 2);
      ctx.stroke();
      ctx.beginPath();
    }

    // render line graph
    drawPolygon(points, '#05c', 2);
    ctx.stroke();

    if ( view.trend ) {
      _drawTrendLine(data, view, 'red');
    }
    ctx.textAlign = 'center';
  }
  else if ( view.type === 'bar' ) {
    const w = Math.max(2, (stepX * 0.9) | 0);
    let maxIndex, maxValue = -Infinity;
    data.forEach((d, i) => {
      if ( d[ view.countCol ] > maxValue ) {
        maxValue = d[ view.countCol ];
        maxIndex = i;
      }
    });
    ctx.fillStyle = '#05c';
    points.forEach((point, i) => {
      if ( view.showMax && maxIndex === i ) ctx.fillStyle = '#09c';
      ctx.fillRect(point.x, _y(minY), w, point.y - _y(minY));
      ctx.fillStyle = '#05c';
    });

    ctx.fillStyle = '#000';
    ctx.textAlign = 'left';
  }

  ctx.strokeStyle = '#fff';
  ctx.lineWidth = 2;

  // render labels
  points.forEach((point, i) => {
    const txt = data[ i ][ view.countCol ].toLocaleString();
    ctx.strokeText(txt, point.x, point.y - 15);
    ctx.fillText(txt, point.x, point.y - 15);
  });

  ctx.setTransform(1, 0, 0, 1, 0, 0);

  function _toWMA(data) {
    const points = [];
    let w = 0;
    for(let i = 1; i <= args.smoothing; i++) w += i;

    for(let i = data.length - 1; i >= 0; i--) {
      let y = 0;
      for(let t = args.smoothing; t > 0; t--) {
        const index = Math.max(0, i - (args.smoothing - t));
        y += data[ index ][ view.countCol ] * t / w;
      }
      points.push(_y(y), i * stepX);
    }

    const smooth = getCurvePoints(points.reverse());
    points.length = 0;
    for(let i = 0; i < smooth.length; i += 2) {
      points.push({ x: smooth[ i ], y: smooth[ i + 1 ] });
    }
    return points;
  }

  function _y(y) {
    return ((maxY - y) * scale) | 0;
  }

  function _drawTrendLine(data, view, color = 'red') {
    const rows = data.length;
    let sum = 0, sum2 = 0;
    for(let i = 1; i <= rows; i++) {
      sum += i;
      sum2 += i * i;
    }
    const sumXY = data.reduce((v, c, i) => v + (i + 1) * c[ view.countCol ], 0);
    const e = data.reduce((v, c) => v + c[ view.countCol ], 0);
    const m = (rows * sumXY - sum * e) / (rows * sum2 - sum * sum);
    const yi = (e - m * sum) / rows;

    const y = (x) => m * x + yi;

    ctx.beginPath();
    let saveStyle = ctx.fillStyle;
    ctx.strokeStyle = ctx.fillStyle = color;
    drawLine(0, _y(y(0)), (rows - 1) * stepX, _y(y(rows - 1)));
    ctx.stroke();

    ctx.fillText((y(0) | 0).toLocaleString(), 0, _y(y(0)) + 12);
    ctx.fillText((y(rows - 1) | 0).toLocaleString(), (rows - 1) * stepX + 30, _y(y(rows - 1)) + 12);
    ctx.fillText(getDiff(y(0), y(rows - 1)), (rows - 1) * stepX + 30, _y(y(rows - 1)) + 24);

    ctx.fillStyle = saveStyle;
  }
}

function addTimestamp(ctx) {
  ctx.font = '12px sans-serif';
  ctx.fillStyle = '#000';
  ctx.textAlign = 'center';
  ctx.textBaseline = 'middle';
  ctx.translate(1900, 540);
  ctx.rotate(-Math.PI * 0.5);
  ctx.fillText(today, 0, 0);
  ctx.setTransform(1, 0, 0, 1, 0, 0);
}

function getMin(min) {
  const digits = min.toString().length;
  const m = Math.pow(10, digits - 1);
  const min2 = Math.ceil(min / m);
  return min2 < 10 ? 0 : min2;
}

function getMax(min) {
  const digits = Math.min(4, min.toString().length);
  const m = Math.pow(10, digits - 1);
  return Math.ceil(min / m) * m;
}

function getStep(min, max) {
  const diff = max - min;
  const digits = diff.toString().length;
  return Math.pow(10, digits - 1);
}

function getDiff(v1, v2) {
  const v = (v2 * 100 / v1) - 100;
  return `${ v < 0 ? '' : '+' }${ v.toFixed(1) }%`;
}

function drawLine(x1, y1, x2, y2) {
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
}

function drawPolygon(points, color = 'red', width = 2) {
  ctx.beginPath();
  for(let i = 0; i < points.length; i++) {
    if ( i === 0 ) ctx.moveTo(points[ 0 ].x, points[ 0 ].y);
    else ctx.lineTo(points[ i ].x, points[ i ].y);
  }
  ctx.strokeStyle = color;
  ctx.lineWidth = width;
  ctx.lineCap = 'round';
  ctx.lineJoin = 'round';
  ctx.stroke();
}

function transpose(data) {
  const keys = Object.keys(data[ 0 ]);
  const values = Object.values(data[ 0 ]);
  const tData = [];
  keys.forEach((key, i) => {
    tData.push({ 'label': key, 'count': values[ i ] });
  });
  return tData;
}

function saveFile(filename) {
  let mime = undefined;
  let options = undefined;

  if ( fileType === 'png' ) {
    mime = 'image/png';
    options = { compressionLevel: 6, filters: canvas.PNG_FILTER_NONE };
  }
  else if ( fileType === 'jpg' || fileType === 'jpeg' ) {
    mime = 'image/jpeg';
    options = { quality: 0.75, chromaSubsampling: false };
  }
  else if ( fileType === 'pdf' ) {
    mime = 'application/pdf';
    options = {
      title       : filename.substr(0, filename.length - 4),
      author      : 'Operator9',
      subject     : 'GabTV Statistics',
      keywords    : 'gab gabtv operator9 statistics stats',
      creator     : 'generategraph.js',
      creationDate: new Date
    };
  }

  const buffer = canvas.toBuffer(mime, options);

  try {
    fs.writeFileSync(path.join(folder, `${ args.date ? args.maxdate + ' ' : '' }${ filename }`), buffer);
  }
  catch(err) {console.error(`Could not save ${ fileType.toUpperCase() } file: ${ err.message }`);}
}

function getCurvePoints(h, t = 0.5, f = 25) {
  if ( typeof h === 'undefined' || h.length < 2 ) {return new Float32Array(0);}
  let j, d = 1, e = h.length, n = 0, m = (e - 2) * f + 2, k = new Float32Array(m),
    a = new Float32Array((f + 2) << 2), b = 4;
  j = h.slice(0);
  j.unshift(h[ 1 ]);
  j.unshift(h[ 0 ]);
  j.push(h[ e - 2 ], h[ e - 1 ]);
  a[ 0 ] = 1;
  for(; d < f; d++) {
    let o = d / f, p = o * o, r = p * o, q = r * 2, s = p * 3;
    a[ b++ ] = q - s + 1;
    a[ b++ ] = s - q;
    a[ b++ ] = r - 2 * p + o;
    a[ b++ ] = r - p;
  }
  a[ ++b ] = 1;
  g(j, a, e, t);

  function g(G, z, B, M) {
    for(let A = 2, H; A < B; A += 2) {
      let C = G[ A ], D = G[ A + 1 ], E = G[ A + 2 ], F = G[ A + 3 ], I = (E - G[ A - 2 ]) * M,
        J = (F - G[ A - 1 ]) * M, K = (G[ A + 4 ] - C) * M, L = (G[ A + 5 ] - D) * M, u = 0, v, w,
        x, y;
      for(H = 0; H < f; H++) {
        v = z[ u++ ];
        w = z[ u++ ];
        x = z[ u++ ];
        y = z[ u++ ];
        k[ n++ ] = v * C + w * E + x * I + y * K;
        k[ n++ ] = v * D + w * F + x * J + y * L;
      }
    }
  }

  e = h.length - 2;
  k[ n++ ] = h[ e++ ];
  k[ n ] = h[ e ];
  return k;
}

function getImage() {
  return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABbCAMAAADEKmKVAAAC+lBMVEUAAAArMTMEERMIEhMDERIQGBkKFBYCDxADERIEERIJFBUMFRcPGhsOFReQl5sCDA4RHB4LFBUBDxAFEBEDDxGfpak8RUgDEBFhaGwVICIADQ8iLC6epKcGERJLU1cOFhcFDxBUXGANGhwsNTissLQRHR8fKStOVlqPlZkgKixwdnqEi48VICKdoqZtdHlFTVFcZGiprbGjqKuRlpuorbCFjJFtdXqcoaVlbXF5gIWBh4mIjpO2ur0uNzpbY2cJFhgRHR8vODqMk5eKkJWVnKB6gYUZJCaxtrpJUVSts7afpKhqcXQdJylKUlYtNzptdHmeo6dVXWBpcXSjp6ukqa0bJSiXnKAoMTOvtLiGjJF3foMoMTSssbSXnKBQV1o1PkFrcndHT1MADQ4EExUBDxECERIADg8DExQJGBoGFRf9/fwIFxgHFhgGFRUMGhwNGx3///8YIyX+/v0PHB77+/sLGBoTHiARHR8UHyEACwwbJigIFRfR0tQXIiQWISMABgfu7/AiLC4eKCr5+fkQHB7GyMrj5eYkLjDl5ujLzc8oMjQeKSsACAn09PTr7O0qNDbe3+C4u774+Pjf4OLo6era3N3U1dfIycz29vfy8vLt7e7Fx8m8vsG0uLs4QUMgKisVICPm6OnO0NLExcjAwsUmMDIaJSfX2NqVm58xOj3w8fHh4uPS09bKy87Bw8ZBSk0sNjji4+XMztBzen5lbG8zPD+mqq2doqWan6OMkpaFi49nbnIGExXW19mrr7N9g4dRWVxETVA9Rkg6Q0Y2P0Lp6uuxtbmorK9XX2JOVllMVFdHT1IgKy0CERT19fbc3d+9wMOfpKeYnaCPlZhqcXRJUVQaJSW+wcS5vcC1ubywtLeTmJySl5peZWkuNzrZ2tytsrWipqqKkJV6gYV4f4J2fYBwd3s/R0qprbFudXliaW1TW1/v8PDBxceCiItgZ2swODuhpaiHjZKAhYpsc3daYmX9/v2prbCXm52QlpuJjpHq6uylqKtbY2ediCvAAAAAYnRSTlMACd8d8kIO+Oa9ZFUtJR6NNRXFdW0S2rWvrp2ZhYBkSjgT+vXy8O7p5ubfzcC6mIV6c2NTREMv+Pby8e3o3NXOyrOfinNzc2FNOC0c/Pv77+rq5+bY08/Dwr26uqmOf204LWFVYtQAABFQSURBVGje3ZlzdCt5FMff2rZt27ZtdzKZSWYy6DRGkzRokrZpatu2bb1XG/tQrG2cs/dXbHe7We9fe1/eOT3BJ3euft872fb/tv33OeSEvQ/Y+4RD9tn/P6Qetd8xJx+9974H4GKx6IAD9t376JOP3e+o/4B76DEHHQhIsQ+YGJNQJPyTkw0HnnDsof+Ku8+xRx+AIxOtskUYRbKCKoUfMJmSU/Y96OCj/rHDRxyoJWkMIxBZ7CMSibZjJCMoFSkad1J7cUll5t5HHPZPwIedvC+rMAgUhpzGgS0WgRENFKM0aEoqO8Z7K5br+84/Yp+/XRCnnQdXrVExlARbDce6qdVqiItcn+xOWnl/+fOckbGLjtnrb5HPuMPZkes0ufQsBtgtJkaxx9iB8bF3R9+pypm/dL+/Dt7riMff721Pcg7wWsT1gt6OQwLk7vna2Zyqru6W0/6q44dd+mX5xz3jSWYKB7I3NEYQOEEQMtVC/einOxMzmk/6axHf77a66rr6H4t5bLvIK1pEYISMwGRaQc9k9X/XNZOYWHP8mX+BfMgFVVXVs+WVLCUhvDstwjFMBkYJel7jbnv3na5ET/dTZ/wp+eAvPIlNu5bdCoGREF69hiokgE3LSWFC43LntrWMNr3T9M6Nh/wZuTk+I3G4wmQ2pyhJKDuvXoPbEopVKFP0Zpc985OFkrGa6qqmpT9mH3KzI3qy6t1it4tPUbASAt+sZ/8NU/tAV1JyhUphUPAaszk501nyZc5sU9VNf1SEZ94cXZjx9XJ9v9OsUrHIazHCBvmr5faVnrGlz5vL3xt3aoPUuIxiLBYLg9qeT17o6Hi/+tNdu54+9Pen0cWF0RnvlL/7XrHTrIdGRNMDkV09dT8kDn1YqMsOLy0tjZysqxSLKIYh5ZSMpkkln1nZ8UlvzsjIt7cf9XudcuLQUNHXY+/9OJ7kNmUBWoaB3/7JtdE6R3zRUEJ8WZouOzLfFmEMHK7ELUrGwjRAcQt6e2Vubm5fTs7styf+Dvp1T+LUzvm+8ZX2ykzThFKO0FjQeFpqdpRO5/hw8kNrdFrB24PhtsCQsM49KQqFSknJZLRFY3cv5H5i6v10bmT36V7JR97g8XTN51bmLiw4kycUDKmlJZhPj60zMPzttHhP17Rnqii+TBcVGRmaGhcrTXTLlSzDsKwyy5Rpd7tdWe8Pz3570/XewnGJZ+fUUqbbbrebknklS1JaLeXT0mkMCYxMi8+YzEiPj4aAQLjDwyPi8oK5xg6CgvGtUBnMycl2u5mX1+eM1r7sBX3f5EzirB2OD7OG51UWhqQoEl+x5UWkhr4dpXOkZ6RH63RRjW/n21I7QwAdxoUmieXgtYqf0GjMLr2CVn3+2ed7fhuSR25NmOluV2RlaYBsUClYOUnixaVh36emlg4WpJUVQg6joiJD822BEXHGvNjgACkXmYmTclbB63kDbxBYli75aE/5xWdtRZ9a6JmpEFLg4JvQZ00AmmWJ/sCAPKPxe1toZLYuzVEYnW4tLBi0fR9njPtgR4CfVMoVKTGSZJTQAwxEkGFFLXvqP75yC/nwc6cSRswCurwsuLoJFStg/SG+wcGxEGvIoyM+3ZqekRHvQH5//0EwIvt+xc3hWkorZyExDEnTNMG+++X8BWdtcdqT0PU+vEWu1GuSB1xmg0VuiuQCwnYAOqQzVJeeUWS1phfqomypgYGdxh1+UrCvuIA+kQyTkRA8OQWVSovb699r+bXbx9069WGNOYUlSUGvgRJxGSwNtZxUGrAjODbWGGFrjC6KB7S1IDSwMy7ug+AwPz8/6Ve+vlwZCyc+rQU+jdAS+svW8eN/deY8kDGV8L6BFxg5oE0IzbrCETogLCbWGBcxmAbgjPjCtKjQQEhijFSK3IaYcB/7YDhGywiYsxIIidhU3F75q9H94vRUtUvDI42hR5UERTru6/uV1M8vJnhH7Affh+sc1vR0azQ0enhgXHBMgNTX11cKD+5DLTAlGJBXzwcC/6TEfdIvyA9XdXs+VmlSLEoVz0MtZWXx8iUOPgl+x4QFQyazHfEZQ+ll4HRqRFxsAITDFwx8D+sQISqB40CHr/BRuTTn/eKkvHd416cleoPColSm8AYE5+Uja2gIKqSyM9talJERXZZti4gIyYsBtC+H2H5+3JIaoXFQP4iM+xAGQXnwJvqlnOFFFZ+iFARBmaLgJ7LMGtUMJ11Hh8WG2LLToq3RMFVtISF5wQFQehy8CBYmrfLBcSAjRYjhSAKQJLsZkcOfaP62lVGpFJBGVrDooXGTzQnIrVV6TF5IYD6qv/iyxtJOSCJKIbyGvjY4YBpkGzoykDxZPfBAc+79swZ/sKV8cUBgBQHGO0migcOb7UWclOPAN/B6R15noC3KAfGAmYfaZQPNBeyI2QnkNYmMdCHSEhJM8rN0uGL8x14oehbAFAwlGDmCQZ/AST8wAtsX+RYSYotMK7QWRkbkQXlsFB63wxYbVqPeKoQkGHbsBvqu4rH27TTDkqhZJRIJDYOBbOKkH/04N8hxq34bA8N1hY6oUmNsDOoWMM43uzbJ2RjQ7L9VUUA6T96Y1Le1f2nCgAdfByWETEaLv+Hi2treH9sFnqNwx0WERhVE5RvDgAsVz3HxPRpCTGTk9fj/Vl3hR2/sKuc725QEeIvhYiRgkJ7Ggpp9Q+2m3N6WisQADnVOZ5TVmlZqjIGy43y54DqNFt6vLbJVBv1WuYkOXM/joY9qnDIo+XWxBHTg+8/HhJooVWZla2tzAcf5Bdt06QnW/FjwGti6fi0FFac2RJUZ1F5E0L6HretH1wQPVJTfTfNvD0ztEZMKhbN4vqK6lAuLCy+Lj4oIRs0SMOLCJajQ/NtsX4u9Cqwj19D3p/DMb15V87qIRFxCU4ymr37PZzOB0ojoBGt4rB/HRc7LJRiAAV0e3gyh9mLr/XipUiH3InWbUht5Nb6doPmkL2rrPm+KzE4oKvX1a/zIrQWXkW9BBk9Bi3f0QWvovWEP/O2L/sv5qTUWf8gJwdh7m+uWixe/LspP3JNkAQW7dmH+zQWFJUFe0Xuvoc/GCC+KVD0QHxnq6dMi/YjLTX317X1VQ7Y9dsiJWg0y0D9IVeEoS7SoverkA9bQB6Le9+J2RWOZLq2qfLxkICu5pK/WETbyUQHnWM41yJWmpP6K5q8TrGlzXsk+ogPX0Ed7R6sb5qInp6YzrENFRY78iFgu372U1BjuSEhsahoqShxKmEwojBrz9y7u12N9mlc0uK34emq6qyvDk24t0qVHD9bJ6xT1QwkeR7RDZ52Kz7DG6wr6AQ1a/teFC08cs74kwivevLYvVXk8nsnCIWthgjXakVbBfNTf3xgemR8aGarTva0rKIiM+iQIOGia/tI5eOLMdR2J+3hD+y81pk8mFKWn6QoGs6PyY3e6td39xbXBeXHGiMDQUlv+YHiprQ4429EB9gu2mMA2unGffUXeAhLUXlZWptPpIuMCIz4wxnJz6i8b65LfDQiI2QECIjDVWAoazcGKcQLIiC3eQEvojRmy19444Q09n/3227Z8W/5o78owzLpJasbhsHzN+fpJ/Yo+TmqbjQvpNHq0Ihyx0QIh3igPmr5j27qdQBDiLTMAfPCfDRwMT/0gtEU74UyEIyHso6JCx8dG7ispZytpoAlRU4xxx3s+BAHoBhrYG8MNp8ifD8cjMOxXEUHbG4YHtaaWRoSFtmHJJe5uQAcEv50WnorOGO6DXIaUUdO+sXUNGAGGURTyev0cowTFMT+vdAD6FRr8kEA7jwY4vpl5N0mfbB/lpGF5EVHRUeFxCG3MZRm8I7izXoTRsM4g3QRo0RoaV/EaWPM28khi8PRmhmUyiQTCp2oZaGj7zqlUmAY5vzBjePSHutIPv3lbyr1tZhlK6EDymqQwQMvAeVTd6CHJ0ly0uYndKdCbaNF2CSmXSwhY8X3kvEKuAOXTzUmHd4enfzln/MJ/LIarYpA0FeEyOKLlMgJpPgg55BMVOOOy37UBhn4UGGIzhbAlUwIpA99lpABglZLpkUrf7RmMb18MqegJDg4AuczIkTQFJ0gQfTI4U8EglWBZmc43frF8nS2QG4nEUaBJi4GhtDIZpmUsSkEpd4Zw33wZaV35PHBsKSwmQak3wF63Kk5JiQxFA5mMRr5r3e5zDvvF9nWnSiBWxwpMZyRqXa4sjcDAZ2AZB+lDq7J9q3pSG8e/S61fzItpFul5JTr/ERu48D4ZmHZ1h9Uk5b6ySYYzLIsnEXg7OEzSjKvS2b5gFkDSYhgpsHKs4cOw7pZBR39d6Ht7QnYsipW8Sk6vS1+ckGlBXWgp2A0ghEl97af/ajs/38RjIig6CalKUajcTudKrj2FAX0oIRkKKmvaWNVbFt9aO1jxUXjcohjUFUvSq7IXvNZC7OD6QHQ1EK75j7csM6csLFgIuCyasvApemdu5ScLmbxFjmFakpI14HhXyA/9k9PF3+k+/i41ZFlNQQZYUgslCtZAUzTEg2KVBobsbS7fsoIdek7lAIlRjFypMWVltrbnlmTCqkSCSIM1BQTKjPGH1g+H+r+JrBjNj5gPkpEgEBmGWguKjKJQSFiFXuUu/+yWw4H3K7dLOjJJVqlQmu0ue3GSM9OkF0gJNOVaM+zMq1qZml6Z01XM5of8GARXxzKCQFISHEclCAmSUQo9n/ze55/d85s7cOe836oRlLw+xWw2wd1DM68gJahUUS9g4u6QT5MSPG3fpIHcCelRQ7ooGehPdOsSvp6kQeOyhomsvt27b3lk21a7oqelGK3EsLzqXcnJGgXSMWA4MvUu4w9JkwnFNQXvVYem9gZBDGi0haIuAbFI0XDBerMp6bPR3fdt+42ddXxvRavCoGQYOWOAPZ2hMRFUOnQnQo90ftoRHd9WE/VedXZjW9BqxkkK0JA/aDFA8wPOyj01OZd4u1H5YM/yUpKSgbBpWUFOYqu6FY1BMP/vImpyPTtLPpvqGynQ5QZBX6P2RmgM/YlRFriDO9b9ac5127zZ5XWfNTvlclYuKCmkiDeGJFjQnu+HFzzvZH423fZNgcOEzlpEX3V6tbRppWGir3vnLsihNzvuktmaxQELrDJ6yM/maAfzrw+pLhnqdu32fDJXYOWD0OICcBwDg7fCaFCqkqoTZy6DcHi16y+szinXMAoFS2Lrox094L9/S+iovata/11T8mxjAqNGXwn0VTQpQfNMKJnrmn4GlbR3uyanZnjMwFgAjaT8pnIJMifOabprqfIceW1BtQ+gwZDjaOOnoUAYZ21X4rnXbvt9u7emZvYLM6tgJNvR8bipfdSGT1S15eL6RZ/3PmpTb57OCM2wDOPcvXN65uptf2Sn1uzatSdTzlAScm09/llLBYm1WniAqg4Sb8pGAvpFYRAq5955p+mqPyQD29P1w2iHlkL7HcQbff5nuho9fmEQLxzQgr51ZOfMD0D+E7sqsWvn7laGAjR47V0ObgaEoCmJomJXU9MuiMaf2gPnvpOT8+4ARaOxBvDfJwMaSi9zebj7nSev2fZX7KFnh0eGc3pZ0G74dvD8932GA1zR2vztSM3zD2/7a3b4qzm7a0fLO2gxJkH9/rs+i+mF+qXP6z66+7htf9muvrBu97c1X3QwSOFuB4ZXpzHN+PLiF2MXrwfjrzp+97fDn1aPlhdrNobJVsOpFHfxx/Vf3Hg5tODfs2svq86prS0vby3J0op81FtdpvSmT9pa36848c0/A3mH37B78d35ln63XtXwy4hDNTOGLHvHSvGJMEP/mT302oVftPT2rdgHFAK1sVPAlMVAXfGu4694a9u/sOOuu/z2x4ozTbyKpSTw6xIYjg6Vs587Zb+1Afqv6NeffsoLF51H0utn5b57H3TEwYfu9R/+IH3ksQfBL9IHHXwk/CT9P7afADzFL2ucTxIdAAAAAElFTkSuQmCC';
}