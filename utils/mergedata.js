#!/usr/bin/env node
/*
 *  Merge data (from various exports) ALPHA
 *
 *  Copyright (c) 2021 operator9, swampcreature
 *  MIT
*/

'use strict';

const today = new Date().toISOString().substr(0, 10);
const yesterday = (new Date(Date.now() - 86400000)).toISOString().substr(0, 10);

const options = require('commander')
  .usage('base.sqlite incremental.sqlite -o merged.sqlite')
  .option('-o, --out <outfile>', 'Save merged data to this file (same type as in-files).')
  .option('-t, --test', 'Test merge, but don\'t save to output.')
  .parse(process.argv);

const args = options.opts();

if ( !args.out || options.args.length !== 2 ) {
  return options.help();
}

const Path = require('path');
const fs = require('fs');
const SQLite = require('better-sqlite3');

const sqlPath = Path.join(__dirname, '..', 'gabtv-data', 'sql');

// determine file type
const ext = Path.extname(args.out).toLowerCase();
if ( ext === '.sqlite' || ext === '.db' || ext === '.sql' ) processDB();
else if ( ext === '.json' ) processJSON();
else if ( ext === '.csv' ) processCSV();
else console.log(`Unknown file type "${ ext }"`);

function processDB() {
  let dbPath;
  if ( args.test ) {
    console.log('****** RUNNING IN TEST MODE *******');
    dbPath = Path.resolve(`/tmp/_mrgtst_${ (Math.random() * 1000000) | 0 }.db`);
  }
  else {
    dbPath = Path.resolve(args.out);
  }

  const db1path = Path.resolve(options.args[ 0 ]);
  const db2path = Path.resolve(options.args[ 1 ]);

  if ( dbPath !== db1path ) {
    fs.copyFileSync(db1path, dbPath);
  }

  // copy of db1 will be treated as base database
  const db1 = new SQLite(dbPath);
  const db2 = new SQLite(db2path, { readonly: true });

  // Alter table if old version
  const columns = db1.prepare('SELECT * from gabtv LIMIT 1').get();
  if ( typeof columns.url === 'undefined' || typeof columns.updated === 'undefined' ) {
    console.log('Updating database to new version.');
    db1.prepare('ALTER TABLE gabtv ADD url').run();
    db1.prepare('ALTER TABLE gabtv ADD updated;').run();
  }

  // add 'deltas' table
  db1.prepare('CREATE TABLE IF NOT EXISTS "deltas" ("date" UNIQUE, "views" INTEGER, PRIMARY KEY("date"))').run();

  const getRecords = db2.prepare('SELECT * FROM gabtv');
  const getViews = db1.prepare('SELECT views FROM gabtv WHERE id = ?');
  const insert = db1.prepare('INSERT INTO gabtv VALUES( @category, @dateISO, @date, @user, @views, @id, @time, @url, @updated )');
  const update = db1.prepare('UPDATE gabtv SET views = ?, url = ?, updated = ? WHERE id = ?');
  const addDelta = db1.prepare('INSERT INTO deltas VALUES( @date, @views )');
  const getTotalViews = db1.prepare('SELECT SUM(views) views FROM gabtv WHERE SUBSTR(date, 0, 11) < ?');

  // get number of views before merge
  const viewCount1 = getTotalViews.get(yesterday).views;

  // progress bar
  const getNumRecs = db2.prepare('SELECT COUNT() count FROM gabtv').get().count;
  let current = 0;

  // insert new/update records
  let added = 0;
  let updated = 0;

  pbar(0, getNumRecs);

  const insertNew = db1.transaction(rows => {
    for(const row of rows) {
      const exists = getViews.get(row.id);
      if ( exists ) {
        const maxViews = Math.max(exists.views, row.views);
        const url = row.url || exists.url;
        const lastUpdated = getLastUpdate(exists.updated, row.updated);
        update.run(maxViews, url, lastUpdated, row.id);
        updated++;
      }
      else {
        insert.run(row);
        added++;
      }
      if ( ++current % 200 === 0 ) pbar(current, getNumRecs);
    }
  });

  insertNew(getRecords.all());

  // get delta per total view count
  const viewCount = getTotalViews.get(today).views - viewCount1;

  // clear pbar
  console.log('\x1b[2K\rCompleted.');

  //noinspection DuplicatedCode
  const sql = [];
  let views;

  // create views
  try {
    fs.readdirSync(sqlPath).forEach(file => {
      //noinspection JSCheckFunctionSignatures,JSUnresolvedFunction
      const query = fs.readFileSync(Path.join(sqlPath, file), 'UTF8')
        .replace(/\r/g, '')
        .split('\n')
        .map(line => line.split('--')[ 0 ])
        .join(' ')
        .replace(/\s+/g, ' ');

      const view = `CREATE VIEW IF NOT EXISTS \`${ file.substr(0, file.length - 4) }\` AS ${ query };`;
      sql.push(view);
    });

    views = sql.join('\n');
  }
  catch(err) {console.log(err);}

  // update delta database
  if ( viewCount1 > 0 ) {
    const date = new Date(db1.prepare('SELECT MAX(dateISO) date FROM gabtv').get().date);
    date.setDate(date.getDate() - 1);
    try {
      addDelta.run({ date: date.toISOString().substr(0, 10), views: viewCount });
    }
    catch {console.log('Deltas already updated.');}
  }

  // insert VIEWs
  db1.exec(views);

  db1.close();
  db2.close();

  console.log(`Added ${ added } new records, updated ${ updated } records. Views delta: ${ viewCount }`);
}

function processJSON() {console.log('Not implemented yet');}

function processCSV() {console.log('Not implemented yet');}

function pbar(current, max, width = 64) {
  const pct = current / max;
  const chars = Math.round(width * pct);
  const dlt = Math.max(0, width - chars);
  console.log(` [\x1b[44m${ ' '.repeat(chars) }\x1b[m${ ' '.repeat(dlt) }] ${ (pct * 100).toFixed(0) }% \x1b[A\r`);
}

function getLastUpdate(d1, d2) {
  d1 = d1 || d2 || (new Date).toISOString();
  d2 = d2 || d1 || (new Date).toISOString();
  return d1 >= d2 ? d1 : d2;
}