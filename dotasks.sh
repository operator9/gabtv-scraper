#!/bin/bash
#
# Example bash script automating production of merging a new dataset, converting to CSV and
# outputting image files. Modify to fit your scenario.
#
# NOTE: You need a Linux BASH shell for this to work or a similar environment (Cygwin, WLS, MacOS etc.).
# and somewhat recent Node.js installed.

#
# --- Be located in the root folder of this project before running (i.e. in the same folder as this file) ---
# --- To enable it, do a first time `chmod +x ./dotasks.sh`
#

# Merge new dataset in home folder with the existing base one in the gabtv-data/ folder
utils/mergedata.js gabtv-data/gabtv-stats-base.sqlite ~/gabtv-stats.sqlite -o gabtv-data/gabtv-stats-base.sqlite

# Convert merged data to CSV in gabtv-data/
utils/sqlite2csv.js gabtv-data/gabtv-stats-base.sqlite gabtv-data/gabtv-stats-base.csv

echo "Generating graph images and PDF..."

# Produce PNG image files of analytics
utils/generategraphs.js gabtv-data/gabtv-stats-base.sqlite -f graphs/images/

# Produce a single page PDF file from the same analytics
utils/generategraphs.js gabtv-data/gabtv-stats-base.sqlite -f graphs/ -t pdf --single
