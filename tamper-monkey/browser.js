// ==UserScript==
// @name         GabTV Scraper
// @namespace    http://tampermonkey.net/
// @version      0.8.3-alpha
// @description  Gab TV stat scraper
// @author       operator9, swampcreature
// @match        https://tv.gab.com/category*
// @grant        none
// @require
// ==/UserScript==

const DEBUG = false;

const second = 1000;
const minute = second * 60;
const hour = minute * 60;
const day = hour * 24;
const month = day * 30;

const delay = 150;
const log = console.log.bind(console);
const storageName = '__op';
const urlMain = 'https://tv.gab.com/category';

const urlSQLiteWASM = 'https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.5.0/sql-wasm.wasm';
const urlSQLiteMin = 'https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.5.0/sql-wasm.min.js';

let page = 1;
let storage;
let timer;

function clog(msg, color = '#fff', header = false) {
  if ( header ) {
    log(`%c${ msg }`, `background-color:#000; padding:3px 7px; border-radius:2px; color:${ color }; font-size:1.2em; font-weight:bold`);
  }
  else {
    log(`%c${ msg }`, `color: ${ color }`);
  }
}

function init() {
  storage = {
    dateStart : null,
    dateEnd   : null,
    dataPoints: [],
    active    : false,
    currentCategory: DEBUG ? 1 : 0,
    dateLimit : Date.now() - month * 12
  };
}

function load() {
  const data = localStorage.getItem(storageName);
  if ( data ) storage = JSON.parse(data);
}

function save() {
  localStorage.setItem(storageName, JSON.stringify(storage));
}

function getLocation() {
  let url = document.location.href.toString();
  if ( url.endsWith('/') ) url = url.substring(0, url.length - 2);
  return url;
}

function getCurrentCategory() {
  const parts = getLocation().split('/');
  const category = parts[ parts.length - 1 ];
  return category === 'category' ? null : category;
}

// main category page only
function getCategoryUrls() {
  let categories = localStorage.getItem(storageName + '_cats');
  if ( categories ) return JSON.parse(categories);
  categories = Array
    .from(document.querySelectorAll('div.uk-flex-center.uk-grid a'))
    .map(n => n.href);

  localStorage.setItem(storageName + '_cats', JSON.stringify(categories));
  return categories;
}

function isActive() {
  return storage.active;
}

function getDataset() {
  let element = document.querySelector('button[data-container]');
  if ( !element || !element.dataset ) element = document.querySelector('[data-container]');
  if ( !element || !element.dataset ) throw 'UI changed. Update process to get dataset.';
  return element.dataset;
}

async function autoPaginate() {
  const dataset = getDataset();
  const container = document.querySelector(dataset.container);
  const slug = dataset.categorySlug;
  const categorySlug = `/category/${ slug }/`;
  page++;

  function done() {
    log('Paginating done. Collecting stats...');
    getStats();
    const urls = getCategoryUrls();
    const url = urls[ storage.currentCategory++ ];
    goto(url ? url : urlMain);
  }

  try {
    const result = await fetch(`${ categorySlug }more?p=${ page }`);
    if ( !result.ok ) return localStorage.setItem('__lastError', 'Not OK (paginate)');

    const html = await result.text();
    if ( !html ) return done();

    // remove image tags to speedup loading and reduce load on server
    container.insertAdjacentHTML('beforeend', html.replace(/<img/gi, '<nop'));

    // check time boundary
    const elements = container.querySelectorAll('div[data-episode-id]');
    const parent = elements[ elements.length - 1 ];
    const lastDate = new Date(toDate(parent.querySelector('span:last-child').innerText)).getTime();
    if ( lastDate < storage.dateLimit ) return done();

    log(`${ slug }: page ${ page }`);

    // check if aborted, if not get next "page"
    if ( isActive() ) setTimeout(autoPaginate, delay);
  }
  catch(err) {
    const msg = `Failed to fetch more episodes: ${ err.message }`;
    localStorage.setItem('__lastError', msg);
    return msg;
  }
}

function getStats() {
  try {
    const category = getCurrentCategory();

    document.querySelectorAll('div[data-episode-id]').forEach(d => {
      const user = d.querySelector('a').href.split('/')[ 4 ];
      const time = stamp2sec(d.querySelector('div.duration-label.no-select').innerText);
      const id = d.dataset.episodeId;
      const url = d.dataset.episodeUrl;

      const spans = d.querySelectorAll('span');
      const views = toViews(spans[ 0 ].innerText);
      const date = spans[ 3 ].innerText;

      // to reduce local storage space which is typically limit to 5 MB, we use shortened aliases
      storage.dataPoints.push({ u: user, v: views, d: date, c: category, id, t: time });

      // store url (full format: "/channel/username/view/title-hash" we only need the last part
      sessionStorage.setItem(id, getVideoUrl(url));
    });

    log('Done');
  }
  catch(err) {
    localStorage.setItem('__lastError', err.message);
    log(`Error: ${ err.message }`);
  }

  function getVideoUrl(url) {
    const parts = url.split('/');
    return parts[ parts.length - 1 ];
  }
}

function goto(url) {
  save();
  setTimeout(() => {document.location.href = url;}, 500);
}

/***********************************
 MAIN
 ***********************************/

window.onload = function() {
  const onMain = getLocation() === urlMain;

  function _done() {
    storage.dateEnd = Date.now();
    storage.active = false;
    save();
    const diff = (storage.dateEnd - storage.dateStart) / minute;
    log(`Completed in ${ diff.toFixed(1) } minutes.`);
  }

  if ( isActive() ) {
    if ( onMain ) {
      const urls = getCategoryUrls();
      if ( storage.currentCategory < urls.length ) {
        const url = urls[ storage.currentCategory++ ];
        if ( DEBUG && storage.currentCategory > 2 ) {
          _done();
          console.log('DEBUG mode - stopping.');
          console.log(storage);
        }
        else {
          goto(url);
        }
      }
      else {
        _done();
        log(`Data size: ${ JSON.stringify(storage).length } bytes`);
        log(`Slug size: ${ JSON.stringify(sessionStorage).length } bytes`);
      }
    }
    else {
      log(`Populating pages... ${ DEBUG ? 'DEBUG MODE' : '' }`);
      autoPaginate()
        .then(msg => {if ( msg && msg.length ) log(msg);})
        .catch(err => log('ERROR:', err));
    }
  }
  else {
    log('\n\n');
    clog('💀 Command Menu ========================= op9 ==', '#c72800', true);
    clog(' timer()        Set/cancel timer running at next midnight.', '#04bcf5');
    clog(' run([limit])   Start collecting stats from category pages.', '#04bcf5');
    clog(' abort()        Abort collection from any page.', '#f1a208');
    clog(' exportSQL()    Export data points as SQLite file.', '#5f9');
    clog(' exportJSON()   Export data points as JSON file.', '#5f9');
    clog(' exportCSV()    Export data points as CSV file.', '#5f9');
    clog('----------------------------------------------------------', '#999');
    clog('run limit: "a day ago" (max 25), "2 months" etc.', '#fc7');
    if ( DEBUG ) log('WARNING: in DEBUG mode.');
  }
};

// limit [optional] '21 days ago' (max 25), 'a month', '3 months ago'.
window.run = function(limit = null) {
  init();
  storage.dateStart = Date.now();
  storage.active = true;
  sessionStorage.clear();

  if ( typeof limit === 'string' && limit.length > 4 ) {
    sessionStorage.setItem('incremental', '1');
    storage.dateLimit = new Date(toDate(limit)).getTime();
    log(`Limited run to ${ limit } (${ storage.dateLimit })`);
  }

  goto(urlMain);
};

window.abort = function() {
  storage.active = false;
  log('ABORTED');
  goto(urlMain);
};

window.timer = function() {
  if ( timer ) {
    clearTimeout(timer);
    timer = null;
    return log('Timer cancelled.');
  }

  const date = new Date();
  const time = date.getTime();
  const midnight = (new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)).getTime();
  const diff = midnight + hour * 24 - time;
  timer = setTimeout(run, diff);

  log(`Timer set. Running in ${ (diff / hour).toFixed(2) } hours.`);
};

window.exportCSV = function() {
  const updated = (new Date(storage.dateEnd)).toISOString();
  const csv = [ 'category;dateISO;date;user;views;id;time;url;updated' ];
  if ( storage.dataPoints.length ) {
    storage.dataPoints.forEach(p => {
      csv.push(`${ p.c };${ toDate(p.d) };${ p.d };${ p.u };${ p.v };${ p.id };${ p.t };${ sessionStorage.getItem(p.id) };${ updated }`);
    });

    exportFile(getFilename('csv'), csv.join('\r\n'));
  }
  else log('Nothing to export.');
};

window.exportJSON = function() {
  if ( storage.dataPoints.length ) {
    const updated = (new Date(storage.dateEnd)).toISOString();
    const clone = JSON.parse(JSON.stringify(storage));
    clone.dataPoints.forEach((p, i) => {
      clone.dataPoints[ i ] = {
        category: p.c,
        dataISO : toDate(p.d),
        date    : p.d,
        user    : p.u,
        views   : p.v,
        id      : p.id,
        time    : p.t,
        url     : sessionStorage.getItem(p.id),
        updated
      };
    });

    const json = JSON.stringify(clone);
    exportFile(getFilename('json'), json);
  }
  else log('Nothing to export.');
};

window.exportSQL = async function() {
  if ( storage.dataPoints.length ) {
    log('Loading sqlite engine...');

    // load wasm file from CDN
    const response = await fetch(urlSQLiteWASM);
    const wasm = await response.blob();

    const script = document.createElement('script');
    script.onerror = () => {log('Could not load SQLite engine from CDN');};
    script.onload = async () => {
      //noinspection JSUnusedGlobalSymbols
      const config = {
        locateFile: () => URL.createObjectURL(wasm)
      };

      //noinspection JSUnresolvedFunction
      initSqlJs(config).then(SQL => {
        log('Creating SQLite database...');

        const updated = (new Date(storage.dateEnd)).toISOString();

        //noinspection JSUnresolvedFunction
        const db = new SQL.Database();
        db.run('CREATE TABLE gabtv (category, dateISO, date, user, views int, id, time int, url, updated);');

        // create index for ids
        db.run('CREATE UNIQUE INDEX id ON gabtv (id)');

        let newVideos = 0;
        storage.dataPoints.forEach(p => {
          try {
            db.run('INSERT INTO gabtv VALUES (?,?,?,?,?,?,?,?,?)', [ p.c, toDate(p.d), p.d, p.u, p.v, p.id, p.t, sessionStorage.getItem(p.id), updated ]);
          }
          catch {
            // error most likely due to existing id, meaning page
            // content slided during scraping = new videos at top
            newVideos++;
          }
        });

        // insert views when scraping full dataset
        if ( !sessionStorage.getItem('incremental') ) db.run(getSQLViews);

        exportFile(getFilename('sqlite'), db.export());
        if ( newVideos ) log(`During the process, ${ newVideos } new videos were added.`);
      });

    };
    script.src = urlSQLiteMin;

    document.getElementsByTagName('head')[ 0 ].appendChild(script);
  }
  else log('Nothing to export.');
};

function getFilename(type) {
  return `gabtv-stats.${ type.toLowerCase() }`;
}

function exportFile(filename, data) {
  const blob = new Blob([ data ], { type: 'application/octet-stream' });
  const href = URL.createObjectURL(blob);
  const a = document.createElement('a');
  Object.assign(a, { href, download: filename });
  a.click();
}

function toDate(txt) {
  let n = 0;
  txt = txt.toLowerCase().trim();
  const num = txt.split(' ')[ 0 ] | 0;

  if ( txt.includes('a few seconds ago') ) {
    n = second;
  }
  else if ( txt.includes('a minute ago') ) {
    n = minute;
  }
  else if ( txt.includes('minutes ago') ) {
    n = num * minute;
  }
  else if ( txt.includes('an hour ago') ) {
    n = hour;
  }
  else if ( txt.includes('hours ago') ) {
    n = num * hour;
  }
  else {
    const d = new Date();
    if ( txt.includes('a day ago') ) {
      d.setDate(d.getDate() - 1);
    }
    else if ( txt.includes('days ago') ) {
      d.setDate(d.getDate() - num);
    }
    else if ( txt.includes('a month ago') ) {
      d.setMonth(d.getMonth() - 1);
    }
    else if ( txt.includes('months ago') ) {
      d.setMonth(d.getMonth() - num);
    }
    d.setSeconds(0);
    const iso = d.toISOString();
    return iso.substr(0, 10) + ' ' + iso.substring(11, 19);
  }
  const iso = new Date(storage.dateStart - n).toISOString();
  return iso.substr(0, 10) + ' ' + iso.substring(11, 16) + ':00';
}

function toViews(txt) {
  let n;
  txt = txt.trim().toLowerCase();
  if ( txt.endsWith('k') ) {
    const parts = txt.split('.').map(p => p.replace(/[^0-9]+/, '') | 0);
    parts[ 1 ] |= 0; // force numeric value in index 1
    n = (parts[ 0 ] + (parts[ 1 ] / Math.pow(10, parts[ 1 ].toString().length))) * 1000;
  }
  else if ( txt.endsWith('m') ) {
    const parts = txt.split('.').map(p => p.replace(/[^0-9]+/, '') | 0);
    parts[ 1 ] |= 0;
    n = (parts[ 0 ] + (parts[ 1 ] / Math.pow(10, parts[ 1 ].toString().length))) * 1e6;
  }
  else {
    n = txt.replace(/[^0-9]+/, '');
  }
  return n | 0;
}

function stamp2sec(txt) {
  const multipliers = [ 1, 60, 3600, 24 * 3600 ];
  return txt
    .split(':')
    .reverse()
    .map(v => v | 0)
    .reduce((n, v, i) => n + v * multipliers[ i ], 0);
}

/***********************************
 INIT
 ***********************************/
init();
load();

// remove image links when collecting data
if ( isActive() ) document
  .querySelectorAll('img')
  .forEach(i => i.removeAttribute('src'));

function getSQLViews() {
  //noinspection SpellCheckingInspection
  return `DROP VIEW IF EXISTS "Misc stats";
CREATE VIEW "Misc stats" AS SELECT COUNT(DISTINCT user) "Total # of posters", COUNT(*) "Total # of videos", SUM(views) "Total # of views", ROUND(AVG(views), 1) "Avg views/video", (SELECT ROUND(AVG(VIEWS),1) FROM gabtv WHERE views < 1500) "Avg views bias adj", MAX(views) "Max views", ROUND(COUNT(*) / 180, 0) "Avg # videos/day", ROUND(COUNT(*) / COUNT(DISTINCT user), 1) "Avg # videos/poster", ROUND(AVG(time) / 60, 1) "Avg playtime (min)", MAX(time) / 60 "Longest video (min)", MIN(time) "Shortest video (sec)", (SELECT COUNT() FROM gabtv WHERE views > 1000) "Videos with > 1K views", (SELECT COUNT() FROM gabtv WHERE views > 10000) "Videos with > 10K views", (SELECT COUNT() FROM gabtv WHERE views = 0) "Videos with no views", (SELECT COUNT() FROM gabtv WHERE views < 10) "Videos with < 10 views", (SELECT COUNT(*) FROM gabtv WHERE time < 30) "# of videos < 30s" FROM gabtv;
DROP VIEW IF EXISTS "Segmented Number of  Videos Per User";
CREATE VIEW "Segmented Number of  Videos Per User" AS SELECT (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 0 AND numOfVideos < 10) "[0, 10>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 10 AND numOfVideos < 20) "[10, 20>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 20 AND numOfVideos < 30) "[20, 30>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 30 AND numOfVideos < 40) "[30, 40>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 40 AND numOfVideos < 50) "[40, 50>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 50 AND numOfVideos < 60) "[50, 60>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 60 AND numOfVideos < 70) "[60, 70>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 70 AND numOfVideos < 80) "[70, 80>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 80 AND numOfVideos < 90) "[80, 90>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 90 AND numOfVideos < 100) "[90, 100>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 100 AND numOfVideos < 110) "[100, 110>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 110 AND numOfVideos < 120) "[110, 120>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 120 AND numOfVideos < 130) "[120, 130>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 130 AND numOfVideos < 140) "[130, 140>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 140 AND numOfVideos < 150) "[140, 150>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 150 AND numOfVideos < 160) "[150, 160>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 160 AND numOfVideos < 170) "[160, 170>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 170 AND numOfVideos < 180) "[170, 180>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 180 AND numOfVideos < 190) "[180, 190>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 190 AND numOfVideos < 200) "[190, 200>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 200 AND numOfVideos < 210) "[200, 210>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 210 AND numOfVideos < 220) "[210, 220>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 220 AND numOfVideos < 230) "[220, 230>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 230 AND numOfVideos < 240) "[230, 240>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 240 AND numOfVideos < 250) "[240, 250>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 250 AND numOfVideos < 260) "[250, 260>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 260 AND numOfVideos < 270) "[260, 270>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 270 AND numOfVideos < 280) "[270, 280>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 280 AND numOfVideos < 290) "[280, 290>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 290 AND numOfVideos < 300) "[290, 300>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 300 AND numOfVideos < 310) "[300, 310>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 310 AND numOfVideos < 320) "[310, 320>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 320 AND numOfVideos < 330) "[320, 330>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 330 AND numOfVideos < 340) "[330, 340>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 340 AND numOfVideos < 350) "[340, 350>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 350 AND numOfVideos < 360) "[350, 360>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 360 AND numOfVideos < 370) "[360, 370>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 370 AND numOfVideos < 380) "[370, 380>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 380 AND numOfVideos < 390) "[380, 390>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 390 AND numOfVideos < 400) "[390, 400>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 400 AND numOfVideos < 410) "[400, 410>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 410 AND numOfVideos < 420) "[410, 420>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 420 AND numOfVideos < 430) "[420, 430>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 430 AND numOfVideos < 440) "[430, 440>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 440 AND numOfVideos < 450) "[440, 450>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 450 AND numOfVideos < 460) "[450, 460>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 460 AND numOfVideos < 470) "[460, 470>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 470 AND numOfVideos < 480) "[470, 480>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 480 AND numOfVideos < 490) "[480, 490>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 490 AND numOfVideos < 500) "[490, 500>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 500 AND numOfVideos < 510) "[500, 510>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 510 AND numOfVideos < 520) "[510, 520>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 520 AND numOfVideos < 530) "[520, 530>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 530 AND numOfVideos < 540) "[530, 540>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 540 AND numOfVideos < 550) "[540, 550>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 550 AND numOfVideos < 560) "[550, 560>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 560 AND numOfVideos < 570) "[560, 570>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 570 AND numOfVideos < 580) "[570, 580>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 580 AND numOfVideos < 590) "[580, 590>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 590 AND numOfVideos < 600) "[590, 600>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 600 AND numOfVideos < 610) "[600, 610>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 610 AND numOfVideos < 620) "[610, 620>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 620 AND numOfVideos < 630) "[620, 630>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 630 AND numOfVideos < 640) "[630, 640>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 640 AND numOfVideos < 650) "[640, 650>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 650 AND numOfVideos < 660) "[650, 660>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 660 AND numOfVideos < 670) "[660, 670>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 670 AND numOfVideos < 680) "[670, 680>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 680 AND numOfVideos < 690) "[680, 690>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 690 AND numOfVideos < 700) "[690, 700>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 700 AND numOfVideos < 710) "[700, 710>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 710 AND numOfVideos < 720) "[710, 720>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 720 AND numOfVideos < 730) "[720, 730>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 730 AND numOfVideos < 740) "[730, 740>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 740 AND numOfVideos < 750) "[740, 750>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 750 AND numOfVideos < 760) "[750, 760>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 760 AND numOfVideos < 770) "[760, 770>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 770 AND numOfVideos < 780) "[770, 780>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 780 AND numOfVideos < 790) "[780, 790>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 790 AND numOfVideos < 800) "[790, 800>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 800 AND numOfVideos < 810) "[800, 810>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 810 AND numOfVideos < 820) "[810, 820>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 820 AND numOfVideos < 830) "[820, 830>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 830 AND numOfVideos < 840) "[830, 840>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 840 AND numOfVideos < 850) "[840, 850>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 850 AND numOfVideos < 860) "[850, 860>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 860 AND numOfVideos < 870) "[860, 870>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 870 AND numOfVideos < 880) "[870, 880>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 880 AND numOfVideos < 890) "[880, 890>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 890 AND numOfVideos < 900) "[890, 900>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 900 AND numOfVideos < 910) "[900, 910>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 910 AND numOfVideos < 920) "[910, 920>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 920 AND numOfVideos < 930) "[920, 930>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 930 AND numOfVideos < 940) "[930, 940>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 940 AND numOfVideos < 950) "[940, 950>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 950 AND numOfVideos < 960) "[950, 960>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 960 AND numOfVideos < 970) "[960, 970>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 970 AND numOfVideos < 980) "[970, 980>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 980 AND numOfVideos < 990) "[980, 990>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 990 AND numOfVideos < 1000) "[990, 1000>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1000 AND numOfVideos < 1010) "[1000, 1010>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1010 AND numOfVideos < 1020) "[1010, 1020>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1020 AND numOfVideos < 1030) "[1020, 1030>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1030 AND numOfVideos < 1040) "[1030, 1040>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1040 AND numOfVideos < 1050) "[1040, 1050>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1050 AND numOfVideos < 1060) "[1050, 1060>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1060 AND numOfVideos < 1070) "[1060, 1070>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1070 AND numOfVideos < 1080) "[1070, 1080>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1080 AND numOfVideos < 1090) "[1080, 1090>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1090 AND numOfVideos < 1100) "[1090, 1100>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1100 AND numOfVideos < 1110) "[1100, 1110>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1110 AND numOfVideos < 1120) "[1110, 1120>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1120 AND numOfVideos < 1130) "[1120, 1130>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1130 AND numOfVideos < 1140) "[1130, 1140>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1140 AND numOfVideos < 1150) "[1140, 1150>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1150 AND numOfVideos < 1160) "[1150, 1160>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1160 AND numOfVideos < 1170) "[1160, 1170>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1170 AND numOfVideos < 1180) "[1170, 1180>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1180 AND numOfVideos < 1190) "[1180, 1190>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1190 AND numOfVideos < 1200) "[1190, 1200>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1200 AND numOfVideos < 1210) "[1200, 1210>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1210 AND numOfVideos < 1220) "[1210, 1220>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1220 AND numOfVideos < 1230) "[1220, 1230>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1230 AND numOfVideos < 1240) "[1230, 1240>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1240 AND numOfVideos < 1250) "[1240, 1250>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1250 AND numOfVideos < 1260) "[1250, 1260>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1260 AND numOfVideos < 1270) "[1260, 1270>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1270 AND numOfVideos < 1280) "[1270, 1280>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1280 AND numOfVideos < 1290) "[1280, 1290>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1290 AND numOfVideos < 1300) "[1290, 1300>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1300 AND numOfVideos < 1310) "[1300, 1310>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1310 AND numOfVideos < 1320) "[1310, 1320>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1320 AND numOfVideos < 1330) "[1320, 1330>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1330 AND numOfVideos < 1340) "[1330, 1340>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1340 AND numOfVideos < 1350) "[1340, 1350>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1350 AND numOfVideos < 1360) "[1350, 1360>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1360 AND numOfVideos < 1370) "[1360, 1370>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1370 AND numOfVideos < 1380) "[1370, 1380>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1380 AND numOfVideos < 1390) "[1380, 1390>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1390 AND numOfVideos < 1400) "[1390, 1400>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1400 AND numOfVideos < 1410) "[1400, 1410>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1410 AND numOfVideos < 1420) "[1410, 1420>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1420 AND numOfVideos < 1430) "[1420, 1430>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1430 AND numOfVideos < 1440) "[1430, 1440>", (SELECT COUNT(user) numOfUsers FROM "Top Users and Number of Videos" WHERE numOfVideos >= 1440 AND numOfVideos < 1450) "[1440, 1450>" FROM gabtv LIMIT 1;
DROP VIEW IF EXISTS "Segmented Views Detailed Lower-End";
CREATE VIEW "Segmented Views Detailed Lower-End" AS SELECT (SELECT count(*) FROM gabtv WHERE views >= 0 AND views < 10) "[0, 10>", (SELECT count(*) FROM gabtv WHERE views >= 10 AND views < 20) "[10, 20>", (SELECT count(*) FROM gabtv WHERE views >= 20 AND views < 30) "[20, 30>", (SELECT count(*) FROM gabtv WHERE views >= 30 AND views < 40) "[30, 40>", (SELECT count(*) FROM gabtv WHERE views >= 40 AND views < 50) "[40, 50>", (SELECT count(*) FROM gabtv WHERE views >= 50 AND views < 60) "[50, 60>", (SELECT count(*) FROM gabtv WHERE views >= 60 AND views < 70) "[60, 70>", (SELECT count(*) FROM gabtv WHERE views >= 70 AND views < 80) "[70, 80>", (SELECT count(*) FROM gabtv WHERE views >= 80 AND views < 90) "[80, 90>", (SELECT count(*) FROM gabtv WHERE views >= 90 AND views < 100) "[90, 100>", (SELECT count(*) FROM gabtv WHERE views >= 100 AND views < 110) "[100, 110>", (SELECT count(*) FROM gabtv WHERE views >= 110 AND views < 120) "[110, 120>", (SELECT count(*) FROM gabtv WHERE views >= 120 AND views < 130) "[120, 130>", (SELECT count(*) FROM gabtv WHERE views >= 130 AND views < 140) "[130, 140>", (SELECT count(*) FROM gabtv WHERE views >= 140 AND views < 150) "[140, 150>", (SELECT count(*) FROM gabtv WHERE views >= 150 AND views < 160) "[150, 160>", (SELECT count(*) FROM gabtv WHERE views >= 160 AND views < 170) "[160, 170>", (SELECT count(*) FROM gabtv WHERE views >= 170 AND views < 180) "[170, 180>", (SELECT count(*) FROM gabtv WHERE views >= 180 AND views < 190) "[180, 190>", (SELECT count(*) FROM gabtv WHERE views >= 190 AND views < 200) "[190, 200>", (SELECT count(*) FROM gabtv WHERE views >= 200 AND views < 210) "[200, 210>", (SELECT count(*) FROM gabtv WHERE views >= 210 AND views < 220) "[210, 220>", (SELECT count(*) FROM gabtv WHERE views >= 220 AND views < 230) "[220, 230>", (SELECT count(*) FROM gabtv WHERE views >= 230 AND views < 240) "[230, 240>", (SELECT count(*) FROM gabtv WHERE views >= 240 AND views < 250) "[240, 250>", (SELECT count(*) FROM gabtv WHERE views >= 250 AND views < 260) "[250, 260>", (SELECT count(*) FROM gabtv WHERE views >= 260 AND views < 270) "[260, 270>", (SELECT count(*) FROM gabtv WHERE views >= 270 AND views < 280) "[270, 280>", (SELECT count(*) FROM gabtv WHERE views >= 280 AND views < 290) "[280, 290>", (SELECT count(*) FROM gabtv WHERE views >= 290 AND views < 300) "[290, 300>", (SELECT count(*) FROM gabtv WHERE views >= 300 AND views < 310) "[300, 310>", (SELECT count(*) FROM gabtv WHERE views >= 310 AND views < 320) "[310, 320>", (SELECT count(*) FROM gabtv WHERE views >= 320 AND views < 330) "[320, 330>", (SELECT count(*) FROM gabtv WHERE views >= 330 AND views < 340) "[330, 340>", (SELECT count(*) FROM gabtv WHERE views >= 340 AND views < 350) "[340, 350>", (SELECT count(*) FROM gabtv WHERE views >= 350 AND views < 360) "[350, 360>", (SELECT count(*) FROM gabtv WHERE views >= 360 AND views < 370) "[360, 370>", (SELECT count(*) FROM gabtv WHERE views >= 370 AND views < 380) "[370, 380>", (SELECT count(*) FROM gabtv WHERE views >= 380 AND views < 390) "[380, 390>", (SELECT count(*) FROM gabtv WHERE views >= 390 AND views < 400) "[390, 400>", (SELECT count(*) FROM gabtv WHERE views >= 400 AND views < 410) "[400, 410>", (SELECT count(*) FROM gabtv WHERE views >= 410 AND views < 420) "[410, 420>", (SELECT count(*) FROM gabtv WHERE views >= 420 AND views < 430) "[420, 430>", (SELECT count(*) FROM gabtv WHERE views >= 430 AND views < 440) "[430, 440>", (SELECT count(*) FROM gabtv WHERE views >= 440 AND views < 450) "[440, 450>", (SELECT count(*) FROM gabtv WHERE views >= 450 AND views < 460) "[450, 460>", (SELECT count(*) FROM gabtv WHERE views >= 460 AND views < 470) "[460, 470>", (SELECT count(*) FROM gabtv WHERE views >= 470 AND views < 480) "[470, 480>", (SELECT count(*) FROM gabtv WHERE views >= 480 AND views < 490) "[480, 490>", (SELECT count(*) FROM gabtv WHERE views >= 490 AND views < 500) "[490, 500>" FROM gabtv LIMIT 1;
DROP VIEW IF EXISTS "Segmented Views";
CREATE VIEW "Segmented Views" AS SELECT (SELECT COUNT(*) FROM gabtv WHERE views < 50) "[0 - 50>", (SELECT COUNT(*) FROM gabtv WHERE views >= 50 AND views < 100) "[50 - 100>", (SELECT COUNT(*) FROM gabtv WHERE views >= 100 AND views < 250) "[100 - 250>", (SELECT COUNT(*) FROM gabtv WHERE views >= 250 AND views < 500) "[250 - 500>", (SELECT COUNT(*) FROM gabtv WHERE views >= 500 AND views < 1000) "[500 - 1000>", (SELECT COUNT(*) FROM gabtv WHERE views >= 1000 AND views < 2500) "[1000 - 2500>", (SELECT COUNT(*) FROM gabtv WHERE views >= 2500 AND views < 5000) "[1000 - 5000>", (SELECT COUNT(*) FROM gabtv WHERE views >= 5000 AND views < 10000) "[5000 - 10000>", (SELECT COUNT(*) FROM gabtv WHERE views >= 10000 AND views < 25000) "[10000 - 25000>", (SELECT COUNT(*) FROM gabtv WHERE views >= 25000 AND views < 50000) "[25000 - 50000>", (SELECT COUNT(*) FROM gabtv WHERE views >= 50000 AND views < 100000) "[50000 - 100000>", (SELECT COUNT(*) FROM gabtv WHERE views >= 100000) "[100000, ->" FROM gabtv LIMIT 1;
DROP VIEW IF EXISTS "Top Categories and Number of Videos";
CREATE VIEW "Top Categories and Number of Videos" AS SELECT category, COUNT(category) numOfVideos FROM gabtv GROUP BY category ORDER BY numOfVideos DESC;
DROP VIEW IF EXISTS "Top Categories and Number of Views";
CREATE VIEW "Top Categories and Number of Views" AS SELECT category, SUM(views) / 1000 numOfViewsK, COUNT(*) numOfVideos, SUM(views) / COUNT(*) avgViewsPerVideo FROM gabtv GROUP BY category ORDER BY numOfViewsK DESC;
DROP VIEW IF EXISTS "Top Users and Number of Videos";
CREATE VIEW "Top Users and Number of Videos" AS SELECT user, COUNT(user) numOfVideos FROM gabtv GROUP BY user ORDER BY numOfVideos DESC;
DROP VIEW IF EXISTS "Total Views and Videos for Top Users Above 100k Views";
CREATE VIEW "Total Views and Videos for Top Users Above 100k Views" AS SELECT user, COUNT(*) videos, date, SUM(views) views FROM gabtv WHERE user IN (SELECT user FROM gabtv WHERE views >= 100000) GROUP BY user ORDER BY views DESC;
DROP VIEW IF EXISTS "Users With Equal or Less Than 5 Videos";
CREATE VIEW "Users With Equal or Less Than 5 Videos" AS SELECT COUNT(user) as "Users With Equal or Less Than 5 Videos" FROM "Top Users and Number of Videos" WHERE numOfVideos <= 5;
DROP VIEW IF EXISTS "Videos Posted per Month";
CREATE VIEW "Videos Posted per Month" AS SELECT substr(dateISO, 0, 8) month, count(*) numOfVideos FROM gabtv GROUP BY month ORDER BY month;
DROP VIEW IF EXISTS "Videos With More Than 100k Views";
CREATE VIEW "Videos With More Than 100k Views" AS SELECT category, user, date, views, time / 60 "time (min)", id FROM gabtv WHERE views >= 100000 ORDER BY views DESC;
DROP VIEW IF EXISTS "Videos With Views Above 100k";
CREATE VIEW "Videos With Views Above 100k" AS SELECT user, views viewsAbove100k, time / 60 mins FROM gabtv WHERE views >= 100000 ORDER BY views DESC;
DROP VIEW IF EXISTS "Videos With Views Above 10k";
CREATE VIEW "Videos With Views Above 10k" AS SELECT user, views viewsAbove100k, time / 60 mins FROM gabtv WHERE views >= 10000 ORDER BY views DESC;
DROP VIEW IF EXISTS "Videos by Gab Employees";
CREATE VIEW "Videos by Gab Employees" AS select substr(dateISO,0,8) month, count() videos from gabtv where user in ('shadowknight412', 'a', 'fosco', 'mgabdev', 'disco') group by month order by month desc;
DROP VIEW IF EXISTS "View Deltas Per Day";
CREATE VIEW "View Deltas Per Day" AS SELECT date Day, views Views FROM deltas ORDER BY date LIMIT 45;
DROP VIEW IF EXISTS "Views Per Last 30 days";
CREATE VIEW "Views Per Last 30 days" AS SELECT SUBSTR(dateISO, 0, 11) Day, SUM(views) Views, COUNT(*) Videos, SUM(views) / COUNT(*) "Avg Views/Video" FROM gabtv WHERE date NOT LIKE '%month%' GROUP BY Day ORDER BY dateISO DESC LIMIT 30;
DROP VIEW IF EXISTS "Views Per Last 45 days";
CREATE VIEW "Views Per Last 45 days" AS SELECT SUBSTR(dateISO, 0, 11) Day, SUM(views) Views, COUNT(*) Videos, SUM(views) / COUNT(*) "Avg Views/Video" FROM gabtv WHERE date NOT LIKE '%month%' GROUP BY Day ORDER BY dateISO DESC LIMIT 46;
DROP VIEW IF EXISTS "Views Per Last 90 days";
CREATE VIEW "Views Per Last 90 days" AS SELECT SUBSTR(dateISO, 0, 11) Day, SUM(views) Views, COUNT(*) Videos, SUM(views) / COUNT(*) "Avg Views/Video" FROM gabtv WHERE date NOT LIKE '%month%' GROUP BY Day ORDER BY dateISO DESC LIMIT 91;
`;
}