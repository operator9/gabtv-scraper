SELECT
  (SELECT COUNT(*) FROM gabtv WHERE views < 50) "[0 - 50>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 50 AND views < 100) "[50 - 100>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 100 AND views < 250) "[100 - 250>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 250 AND views < 500) "[250 - 500>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 500 AND views < 1000) "[500 - 1000>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 1000 AND views < 2500) "[1000 - 2500>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 2500 AND views < 5000) "[1000 - 5000>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 5000 AND views < 10000) "[5000 - 10000>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 10000 AND views < 25000) "[10000 - 25000>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 25000 AND views < 50000) "[25000 - 50000>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 50000 AND views < 100000) "[50000 - 100000>",
  (SELECT COUNT(*) FROM gabtv WHERE views >= 100000) "[100000, ->"
FROM gabtv LIMIT 1
