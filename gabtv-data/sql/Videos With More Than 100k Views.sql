SELECT
  category,
  user,
  date,
  views,
  duration / 60 "time (min)",
  id
FROM gabtv
WHERE
  views >= 100000
ORDER BY
  views DESC;
