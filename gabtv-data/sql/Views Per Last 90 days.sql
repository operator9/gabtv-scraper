SELECT
  SUBSTR(date, 0, 11) Day,
	SUM(views) Views,
	COUNT() Videos,
	SUM(views) / COUNT() "Avg Views/Video"
FROM gabtv
WHERE date NOT LIKE '%month%'
GROUP BY Day
ORDER BY date DESC
LIMIT 91;
