SELECT
  user,
  COUNT() videos,
  date,
  SUM(views) views
FROM gabtv
WHERE
  user IN (SELECT user FROM gabtv WHERE views >= 100000)
GROUP BY user
ORDER BY views DESC;
